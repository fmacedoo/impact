﻿using CRM.DB;
using CRM.Repository.Base;
using CRM.Repository.Interface;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Business
{
    public static class GeneralManager
    {
        public static void Initialize()
        {
            SingleContainer.Instance.InitializeContainer();
            EFUnitOfWorkFactory.SetObjectContext(() => new ImpactEntities());
        }

        public static void Commit()
        {
            UnitOfWork.Commit();
        }

        public static void Dispose()
        {
            UnitOfWork.Current.Dispose();
        }
    }
}
