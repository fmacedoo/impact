﻿using CRM.DB;
using CRM.Repository.Base;
using CRM.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CRM.Business
{
    public class AccountManager
    {
        private static IRepository<Account> _Repository
        {
            get
            {
                return SingleContainer.Instance.Container.GetInstance<IRepository<Account>>();
            }
        }

        public static bool AddOrUpdate(Account account)
        {
            try
            {
                if (account.AccountId == default(int))
                {
                    _Repository.Add(account);
                }
                else
                {
                    _Repository.Update(account);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void Remove(int id)
        {
            ContactManager.RemoveByAccount(id);
            _Repository.Delete(new Account { AccountId = id });
        }

        // Bring a single Account object from the database.
        public static Account Single(int id)
        {
            return _Repository.Single(o => o.AccountId == id);
        }

        // Bring all Accounts from the database.
        public static IEnumerable<Account> GetAll()
        {
            return _Repository.GetAll();
        }
    }
}
