﻿using CRM.DB;
using CRM.Repository.Base;
using CRM.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using CRM.Business.Util;

namespace CRM.Business
{
    public class ContactManager
    {
        private static IRepository<Contact> _Repository
        {
            get
            {
                return SingleContainer.Instance.Container.GetInstance<IRepository<Contact>>();
            }
        }

        /// <summary>
        /// Update a Contact's property with the parameter value.
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Contact UpdateContact(ContactBindInfo contactBindInfo)
        {
            if (contactBindInfo.ContactId != default(int))
            {
                var contact = _Repository.Single(o => o.ContactId == contactBindInfo.ContactId);
                setContactValue(contactBindInfo.Field, contactBindInfo.Value, contact);
                _Repository.Update(contact);
                return contact;
            }
            else
            {
                var contact = new Contact();
                setContactValue(contactBindInfo.Field, contactBindInfo.Value, contact);

                contact.AccountId = contactBindInfo.AccountId;

                _Repository.Add(contact);
                return contact;
            }
        }

        public static IEnumerable<Contact> GetContacts(int accountId)
        {
            return _Repository
                .GetQuery()
                .Where(o => o.AccountId == accountId);
        }

        public static void Remove(int contactId)
        {
            Remove(new Contact { ContactId = contactId });
        }

        public static void Remove(Contact contact)
        {
            _Repository
                .Delete(contact);
        }

        public static void RemoveByAccount(int accountId)
        {
            var contacts = _Repository
                .GetQuery()
                .Where(o => o.AccountId == accountId);

            foreach (var contact in contacts)
            {
                Remove(contact);
            }
        }

        private static void setContactValue(string field, string value, Contact contact)
        {
            // Get PropertyInfo that the property name match field parameter
            var propertyInfo = contact.GetType().GetProperty(field.ToUpper(), System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);

            // Set value into property encapsulated in propertyInfo
            // I know that code only works if the property's type is string. In the future i can change that code to something more generic.
            propertyInfo.SetValue(contact, value);
        }
    }
}
