﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Business.Util
{
    /// <summary>
    /// Represents the information necessary to bind a value into a Contact's class property.
    /// </summary>
    public class ContactBindInfo
    {
        public int AccountId { get; set; }
        public int ContactId { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
    }
}
