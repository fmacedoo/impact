﻿using CRM.Business;
using CRM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM.Controllers
{
    public class AccountController : ApiController
    {
        public Account Get(int id)
        {
            return AccountManager.Single(id);
        }

        public IEnumerable<Account> GetAll()
        {
            return AccountManager.GetAll();
        }

        /*
         * The HttpGet is needed because according to route config any url without "Get" prefix need to be marked.
         */
        [HttpGet]
        public Contact[] ListContactByAccount(int id)
        {
            return ContactManager.GetContacts(id).ToArray();
        }

        public HttpResponseMessage Post([FromBody]Account account)
        {
            var isSuccess = AccountManager.AddOrUpdate(account);
            return Request.CreateResponse(isSuccess ? HttpStatusCode.OK : HttpStatusCode.NotFound);
        }

        public void Delete(int id)
        {
            AccountManager.Remove(id);
        }
    }
}
