﻿using CRM.Business;
using CRM.Business.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM.Controllers
{
    public class ContactController : ApiController
    {
        public int Post([FromBody]ContactBindInfo contactBindInfo)
        {
            var contact = ContactManager.UpdateContact(contactBindInfo);

            // I do it here because i need to get the id information before the json object is dispatched.
            GeneralManager.Commit();

            return contact.ContactId;
        }

        public void Delete(int id)
        {
            ContactManager.Remove(id);
        }
    }
}
