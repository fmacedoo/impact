﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace CRM
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);


            // To support extras GET and POST methods

            config.Routes.MapHttpRoute(
                name: "DefaultApiWithId",
                routeTemplate: "Api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }, 
                constraints: new { id = @"\d+" });

            config.Routes.MapHttpRoute(
                name: "DefaultApiWithAction",
                routeTemplate: "Api/{controller}/{action}");

            config.Routes.MapHttpRoute(
                name: "DefaultApiGet",
                routeTemplate: "Api/{controller}",
                defaults: new { action = "Get" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });

            config.Routes.MapHttpRoute(
                name: "DefaultApiPost",
                routeTemplate: "Api/{controller}",
                defaults: new { action = "Post" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            // Remove o formatador XML principal
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            #if DEBUG
            // No caso de DEBUG, indentar os retornos JSON
            config.Formatters.JsonFormatter.Indent = true;
            #endif

            // Formata as propriedades JSON no padrão
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
