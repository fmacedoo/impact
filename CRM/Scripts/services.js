﻿var impactServices = angular.module('impact.services', []);

impactServices.config(["$provide", function ($provide) {

    // Gets from the head some links that will be used in calls to the server.
    $provide.constant("URLaccount", $("#linkAccount").attr("href"));
    $provide.constant("URLaccountGetAll", $("#linkAccountGetAll").attr("href"));
    $provide.constant("URLaccountListContactByAccount", $("#linkAccountListContactByAccount").attr("href"));
    $provide.constant("URLcontact", $("#linkContact").attr("href"));
}]);

impactServices.factory('impactAPIservice', ["$http", "URLaccount", "URLaccountGetAll", "URLaccountListContactByAccount", "URLcontact", function ($http, URLaccount, URLaccountGetAll, URLaccountListContactByAccount, URLcontact) {

    var impactAPI = {};
    
    impactAPI.getAccount = function (id) {
        return $http({
            url: URLaccount + "?id=" + id
        });
    }

    impactAPI.getAccounts = function () {
        return $http({
            url: URLaccountGetAll
        });
    }

    impactAPI.postAccount = function (account) {
        return $http({
            method: 'POST',
            url: URLaccount,
            data: account
        });
    }

    impactAPI.deleteAccount = function (accountId) {
        return $http({
            method: 'DELETE',
            url: URLaccount + "/" + accountId
        });
    }

    impactAPI.listContactByAccount = function (id) {
        return $http({
            url: URLaccountListContactByAccount + "?id=" + id
        });
    }

    impactAPI.postContact = function (accountId, contactId, field, value) {
        return $http({
            method: 'POST',
            url: URLcontact,
            data: {
                accountId: accountId,
                contactId: contactId,
                field: field,
                value: value
            }
        });
    }

    impactAPI.deleteContact = function (contactId) {
        return $http({
            method: 'DELETE',
            url: URLcontact + "/" + contactId
        });
    }

    return impactAPI;
}]);