﻿var impact = angular.module('impact', [
    'xeditable',
    'ngRoute',
    'impact.services',
    'impact.controllers'
]);

impact.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/account', {
                templateUrl: 'partials/account-list.html',
                controller: 'accountListCtrl',
                title: 'Account List'
            }).
            when('/account/create', {
                templateUrl: 'partials/account-manager.html',
                controller: 'accountManagerCtrl',
                title: 'Creating Account'
            }).
            when('/account/:accountId', {
                templateUrl: 'partials/account-manager.html',
                controller: 'accountManagerCtrl',
                title: 'Editing Account'
            }).
            otherwise({
                redirectTo: '/account'
            });
    }
]);

impact.run(['$rootScope',
    function ($rootScope) {
        $rootScope.$on("$routeChangeSuccess", function (event, currentRoute, previousRoute) {
            $rootScope.title = currentRoute.title;
        });
    }
]);
