﻿var impactControllers = angular.module('impact.controllers', []);

impactControllers.controller('menuCtrl', ['$scope', '$location', function ($scope, $location) {
    $scope.activeClass = function (page, isRegExp, hide) {
        var hideclass = hide ? 'hide' : '';
        var currentRoute = $location.path().substring(1) || 'home';
        if (isRegExp) {
            return new RegExp(page).exec(currentRoute) ? 'active' : hideclass;
        } else {
            return page === currentRoute ? 'active' : hideclass;
        }
    };
}]);

impactControllers.controller('TabsCtrl', ['$scope', function ($scope) {

    var defaultTab = null;

    $scope.tabs = [defaultTab = {
        title: 'Basic',
        url: 'partials/account-tabs/edit.html'
    }, {
        title: 'Contacts',
        url: 'partials/account-tabs/contacts.html'
    }];

    // Set the current tab with the first tab url
    $scope.currentTab = defaultTab.url;

    $scope.onClickTab = function (tab) {
        $scope.currentTab = tab.url;
    }

    $scope.isActiveTab = function (tabUrl) {
        return tabUrl == $scope.currentTab;
    }
}]);

impactControllers.controller('accountListCtrl', function ($scope, $http, impactAPIservice) {

    $scope.accountsList = [];

    $scope.deleteAccount = function (account) {
        impactAPIservice.deleteAccount(account.accountId).success(function () {

            // Removes from accountsList the account object that was just deleted
            $scope.accountsList.splice($scope.accountsList.indexOf(account), 1);

        });
    }

    impactAPIservice.getAccounts().success(function (accounts) {
        $scope.accountsList = accounts;
    });
});

impactControllers.controller('accountManagerCtrl', function ($scope, $routeParams, $location, impactAPIservice) {

    $scope.createOrUpdate = function () {
        impactAPIservice.postAccount($scope.account).success(function () {
            $location.path('/accounts');
        });
    };

    $scope.cancel = function () {
        $location.path('/accounts');
    };

    $scope.createNewUser = function () {
        $location.path('/account/create');
    };

    $scope.updateContact = function (field, contact) {
        var newValue = contact[field];
        impactAPIservice.postContact($scope.account.accountId, contact.contactId, field, newValue).success(function (contactId) {

            // If contactId is undefined i push a new object to the collection
            if (!contact.contactId) {
                $scope.contacts.push({});
            }

            contact.contactId = contactId;
        });
    }

    $scope.deleteContact = function (contact) {
        impactAPIservice.deleteContact(contact.contactId).success(function () {

            // Removes from contacts the contact object that was just deleted
            $scope.contacts.splice($scope.contacts.indexOf(contact), 1);

        });
    }

    // if route parameter accountId come with a value it means that an existing object will be updated.
    if ($routeParams.accountId) {

        // Sets the button text perfming a update
        $scope.createOrUpdateButtonLabel = 'Update account';

        impactAPIservice.getAccount($routeParams.accountId).success(function (account) {
            $scope.account = account;
        });

        impactAPIservice.listContactByAccount($routeParams.accountId).success(function (contacts) {

            // Inserts a new empty object to the contacts collection just to offer a new blank line to user input
            contacts.push({});

            $scope.contacts = contacts;
        });

    } else {
        // if not, a new object will be created
        $scope.createOrUpdateButtonLabel = 'Create account';
    }

});
