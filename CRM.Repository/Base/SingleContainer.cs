﻿using CRM.Repository.Interface;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Repository.Base
{
    /// <summary>
    /// Represents a singleton that encapsulate a default instance of StructureMap.Container
    /// </summary>
    public class SingleContainer
    {
        private static SingleContainer _Instance;

        private SingleContainer() { }

        public void InitializeContainer()
        {
            if (Container == null)
            {
                Container = new Container(c =>
                    {
                        c.For<IUnitOfWorkFactory>().Use<EFUnitOfWorkFactory>();
                        c.For(typeof(IRepository<>)).Use(typeof(EFRepository<>));
                    }
                );
            }
        }

        public Container Container { get; private set; }

        public static SingleContainer Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new SingleContainer();
                }

                return _Instance;
            }
        }
    }
}
