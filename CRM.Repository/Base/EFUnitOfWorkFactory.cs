﻿using CRM.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Repository.Base
{
    /// <summary>
    /// Represents a default factory for the EFUnitOfWork type.
    /// </summary>
    public class EFUnitOfWorkFactory : IUnitOfWorkFactory
    {
        private static Func<DbContext> _objectContextDelegate;
        private static readonly Object _lockObject = new object();

        /// <summary>
        /// Sets a delegate that must retrieve a valid DbContext.
        /// </summary>
        /// <param name="dbContextDelegate"></param>
        public static void SetObjectContext(Func<DbContext> dbContextDelegate)
        {
            _objectContextDelegate = dbContextDelegate;
        }

        /// <summary>
        /// Create and returns a new EFUnitOfWork.
        /// </summary>
        /// <returns></returns>
        public IUnitOfWork Create()
        {
            DbContext context;

            lock (_lockObject)
            {
                context = _objectContextDelegate();
            }

            return new EFUnitOfWork(context);
        }
    }
}
