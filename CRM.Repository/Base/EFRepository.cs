﻿using CRM.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Repository.Base
{
    /// <summary>
    /// Represents a default and base structure to manipulate data across the database.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EFRepository<T> : IRepository<T> where T : class
    {
        private DbContext _context;
        private IDbSet<T> _objectSet;

        protected DbContext Context
        {
            get
            {
                if (_context == null)
                {
                    _context = GetCurrentUnitOfWork<EFUnitOfWork>().Context;
                }

                return _context;
            }
        }

        protected IDbSet<T> DbSet
        {
            get
            {
                if (_objectSet == null)
                {
                    _objectSet = this.Context.Set<T>();
                }

                return _objectSet;
            }
        }

        public TUnitOfWork GetCurrentUnitOfWork<TUnitOfWork>() 
            where TUnitOfWork : IUnitOfWork
        {
            return (TUnitOfWork)UnitOfWork.Current;
        }

        public IQueryable<T> GetQuery()
        {
            return DbSet;
        }

        public IEnumerable<T> GetAll()
        {
            return GetQuery().ToList();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> where)
        {
            return DbSet.Where<T>(where);
        }

        public T Single(Expression<Func<T, bool>> where)
        {
            return DbSet.SingleOrDefault<T>(where);
        }

        public T First(Expression<Func<T, bool>> where)
        {
            return DbSet.First<T>(where);
        }

        public virtual void Delete(T entity)
        {
            // All objects that will be removed must be attached before the method Remove.
            var entry = Context.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                Attach(entity);
            }

            DbSet.Remove(entity);
        }

        public virtual void Add(T entity)
        {
            DbSet.Add(entity);
        }

        public void Attach(T entity)
        {
            DbSet.Attach(entity);
        }

        public void Update(T entity)
        {
            // All objects that will be modified must be attached before it have its state changed to Modified.
            Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }
    }
}
